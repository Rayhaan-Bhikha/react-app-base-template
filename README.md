## Getting started

To start the applicaiton in a dev environent, you must be in the root directory of the repository and run the following command:
```**docker-compose up -d --build**```

This should start two docker containers:
- `kaizen-frontend` on port 8080
- `kaizen-backend` on port 8081

To access the application, goto [localhost:8080](http://localhost:8080)

## Errors with kazien-backend not running
This is going to be resolved however for now you can do the following workaround locally:
- Navigate to the `server` directory and run `yarn install`.

