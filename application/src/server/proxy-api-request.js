import express from 'express'
import axios from 'axios'
const server = express.Router();
import bodyParser from 'body-parser'

server.use(bodyParser.urlencoded({
    extended: false
}))

server.use(bodyParser.json({
    strict: true
}))

// url configurations pointing to dedicated node api server
var urlConfig = {
    protocol: "http",
    host: process.env.apiURL || 'localhost',
    port: 8000,
    baseUrl: "/api/",
}

var Api = axios.create({
    baseURL: `${urlConfig.protocol}://${urlConfig.host}:${urlConfig.port}${urlConfig.baseUrl}`,
    timeout: 2000,
    headers: {
        "Content-Type": "application/json;charset=utf-8"
    }
});

// redirect all api request to dedicated node server.
server.all("/*", async (req, res) => {
    console.log(`PROXY SERVER USED!! ---> to host: ${process.env.apiURL}`)
    try {
        let response = await proxyRequest(req);
        res.status(200).send(response.data);
    } catch (error) {
        console.log(error);
        res.status(400).send("There was an error");
    }
})

async function proxyRequest(req) {
    if (req.method === 'GET') {
        let response = await Api.get(req.path);
        return response;
    } else if (req.method === 'POST') {
        let response = Api.post(req.path, req.body)
        return response;
    } else {
        return "METHOD NOT HANDLED!"
    }
}

export default server;