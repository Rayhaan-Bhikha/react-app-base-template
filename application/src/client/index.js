import React from 'react'
import {render, hydrate} from 'react-dom'
import {HashRouter, BrowserRouter} from 'react-router-dom'

import {App} from 'Components'

/* hydrate instead of render when in production mode. */
if(process.env.NODE_ENV === 'production') {
  hydrate(
    <BrowserRouter>
      <App />
    </BrowserRouter>, 
    document.getElementById('app'));
} else {
  render(
    <HashRouter>
      <App />
    </HashRouter>, 
    document.getElementById('app'));
}