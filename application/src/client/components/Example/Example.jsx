import React from 'react'
import {Api} from 'Services'

export default class Example extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            userData: null,
            apiError: false
        }
    }

    requestUsers = async () => {
        try {
            const response = await Api.get("/users");
            const data = response.data;
            this.setState({
                userData: data,
                apiError: false
            })
        } catch(error) {
            console.log("Rayhaan", error);
            this.setState({
                apiError: true
            })
        }
    }

    getUsers = () => {
        if(this.state.userData != null) {
            const usersList = this.state.userData.map(user => 
                <li key={user.id}>
                    <span>name: {user.name}</span><span>   gender: {user.gender}</span>
                </li>
            );
            return usersList
        }
        return null;
    }

    resetState = () => {
        this.setState({
            apiError: false,
            userData: null
        })
    }

    render() {
        return (
            <div>
                <h2>Example Api:</h2>
                <button onClick={this.requestUsers}> Get Users </button>
                <ul>{this.getUsers()}</ul>
                {this.state.apiError && <p>There was an error with the api!</p>}
                <button onClick={this.resetState}>Reset</button>
            </div>
        )
    }
}