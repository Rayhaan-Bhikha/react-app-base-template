import React from "react";
import Enzyme from "enzyme";

import Example from "./Example.jsx";

describe("Welcome", () => {
  it("H2 tag contains 'Example Api'", () => {
    const wrapper = Enzyme.shallow(<Example />);
    expect(wrapper.contains(<h2>Example Api:</h2>));
  });
});
