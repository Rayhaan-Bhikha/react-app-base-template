import React from 'react'
import { Route, Switch} from 'react-router-dom'

import {Home, Example} from 'Components'

export default class App extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        return (
          <Switch>
            <Route exact path = "/" component = {Home}/>
            <Route path="/example" component={Example}/>
          </Switch>
        )
    }
};