import axios from 'axios'

var baseURL = null;

switch (process.env.NODE_ENV) {
    case 'development':
        baseURL='http://localhost:8000/api/';
        break;
    case 'production':
        baseURL = '/api/';
        break;
}

console.log(process.env.NODE_ENV, baseURL);

var Api = axios.create({
    baseURL: baseURL
})

/*
    if in development mode 
        - api should talk directly to the api node server.
    if in production mode
        - api request should proxy through node server which in turn talks to the api server.

    only thing that changes in the baseURL.
    
    in Prod
        we provide 'absolute' base url which is /api/.
    in Dev.
        we provide 'absolute' url and it should be http: //localhost:8081.
*/

export default Api;