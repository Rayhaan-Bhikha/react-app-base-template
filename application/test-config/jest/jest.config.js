module.exports = {
    "verbose": true,
    "testEnvironment": "node",
    "transform": {
      "^.+\\.jsx$": "babel-jest"
    },
    "globals": {
      "NODE_ENV": "test"
    },
    "moduleFileExtensions": [
      "js",
      "jsx"
    ],
    "moduleDirectories": [
      "node_modules",
      "<rootDir>/src/client" // only client directory for now
    ],
    "moduleNameMapper":{
        "Components": "<rootDir>/src/client/components/",
        "Services": "<rootDir>/src/services/"
    },
    "setupTestFrameworkScriptFile": "<rootDir>/test-config/enzyme/enzyme-setup.js"
  }
