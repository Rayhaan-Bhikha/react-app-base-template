const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');

const SRC_DIR = path.resolve("src");
const DIST_DIR_OUTPUT = path.resolve(__dirname, "..", 'dist');

const config = {
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                options: {
                    presets: ["env", "react", "stage-2"],
                    plugins: [require("babel-plugin-transform-class-properties")]
                }
            },
            {
                test: /\.scss$/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader" // compiles Sass to CSS
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(
            [
                DIST_DIR_OUTPUT
            ], 
            {
                allowExternal: true
            }
        )
    ],
    resolve: {
        alias: {
            Components: path.join(SRC_DIR, "client", "components"),
            Services: path.join(SRC_DIR, "client", "services")
        }
    }
}

module.exports = config;