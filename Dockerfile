# Pull node image from docker hub - stage
# ==========================
FROM node:8.11.4 as base

# front end Stage
# ==========================
FROM base as kaizen-application

# RUN useradd -ms /bin/bash kaizen-application

# USER kaizen-application

# set working directory.
WORKDIR /home/kaizen-application

# copy src test package.json etc.
COPY  ./application ./

# install dependencies
RUN yarn install

# clean up


# Back end Stage
# ==========================
FROM base as kaizen-api-server

# RUN useradd -ms /bin/bash kaizen-api-server

# USER kaizen-api-server

# set working directory.
WORKDIR /home/kaizen-api-server

# copy src test package.json etc.
COPY  ./api-server ./

# install dependencies
RUN yarn install

# clean up


# Production stage.
# =========================
FROM kaizen-application as kaizen-production-app

RUN yarn build:prod
