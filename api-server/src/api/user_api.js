const path = require('path');
const fs = require('fs');

const {promisify} = require('util');

const readFile = promisify(fs.readFile);
const writeFile = promisify(fs.writeFile);

const DB = path.join(__dirname, "..", "db")
const USER_DB = path.join(DB, "users.json")

async function getUsersFromDB() {
    let data = await readFile(USER_DB)
    let userData = JSON.parse(data);
    return userData;
}

async function addUsersToDB({name, gender}) {
    let users = await getUsersFromDB()
    let id = users.length + 1;
    users.push({
        id,
        name,
        gender
    });
    console.log("UPDATED USERS: ", users);
    await writeFile(USER_DB, JSON.stringify(users));
    return `${name} added successfully!`;
}

module.exports = {
    getUsersFromDB,
    addUsersToDB
}

/*
===== Example usage =======
readFromDB()
    .then(console.log);

var user = {
    name: 'Theresa May',
    gender: 'female'
}

appendToDB(user)
    .then(console.log)
*/
