const express = require('express');
const app = express.Router();
const {
    getUsersFromDB,
    addUsersToDB
} = require("./user_api");

const errorMessage = "Sorry there was an error, please try again";

app.get("/users", async (req, res) => {
    try {
        let response = await getUsersFromDB();
        res.status(200).json(response);
    } catch (error) {
        console.log(`Error: ${error.message}`);
        res.status(400).send({
            message: errorMessage
        })
    }
})

app.post("/users", async (req, res) => {
    console.log("RESPONSE: ", req.body);
    try {
        let response = await addUsersToDB(req.body.user);
        res.status(200).send({
            message: response
        })
    } catch (error) {
        console.log(`Error: ${error.message}`);
        res.status(400).send({
            message: errorMessage
        })
    }
})

module.exports = app;