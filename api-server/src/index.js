const express = require('express');
const app = express();
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({
    extended: false
}))

app.use(bodyParser.json({
    strict: true
}))

const port = process.env.PORT || 8000;

app.use(cors());

app.use('/api', require('./api'));

app.all('/*', (req, res) => {
    res.status(404).send("Page does not exist");
})

app.listen(port, () => {
    console.log(`server started on port: ${port}`);
})